<?php

namespace App\Http\Controllers\admin;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $orders = Order::all();
        return view('admin.orders.index', compact('orders'));
    }

    public function removeProduct(Order $order, Product $product){
        $order->products()->detach($product->id);
        session()->flash('status', 'Product successfully removed');
        return back();
    }
}
