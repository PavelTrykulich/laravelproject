<div class="col-md-12">
    <h1>Latest posts</h1>
    <div class="row">
        @foreach($latestPosts as $post)
            <div class="col-md-4">
                <h2>{{ $post->title }}</h2>
                <p>{{ $post->short_description }}</p>
                <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
            </div>
        @endforeach
    </div>
</div>