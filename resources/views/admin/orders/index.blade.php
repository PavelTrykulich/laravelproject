@extends('layouts.admin')

@section('main_content')
    <div class="col-md-12">
        <table class="table">
            <tr>
                <th>id</th>
                <th>name</th>
                <th>email</th>
                <th>phone</th>
                <th>Products</th>
            </tr>

            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->phone}}</td>
                    <td>
                        <ul>
                            @foreach($order->products as $product)
                                <li>
                                    {{$product->title}}
                                     x {{$product->pivot->amount}}

                                    <form style="display: inline-block"  method="post" action="/admin/orders/{{$order->id}}/{{$product->slug}}">
                                        {{method_field('delete')}}
                                        {{csrf_field()}}
                                        <button class="btn btn-danger"
                                                type="submit">X</button>
                                    </form>
                                </li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection