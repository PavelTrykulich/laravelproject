@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">Create Post:</h1>
    <p>Here we can create new post</p>
@endsection

@section('main_content')

    <form action="/posts" method="post" enctype="multipart/form-data">

        @include('layouts.embed.errors')

        {{csrf_field()}}

        {{method_field('put')}}

        <div class="form-group">
            <label for="title">Title:</label>
            <input class="form-control" value="{{old('title')}}" type="text" name="title" id="title">
        </div>

        <div class="form-group">
            <label for="slug">Slug:</label>
            <input class="form-control" value="{{old('slug')}}" type="text" name="slug" id="slug">
        </div>

        <div class="form-group">
            <label for="image">Upload image:</label>
            <input class="form-control" value="{{old('image')}}" type="file" name="image" id="image">
        </div>

        <div class="form-group">
            <label for="short_description">Short description:</label>
            <textarea class="form-control"  name="short_description" id="short_description">{{old('short_description')}}</textarea>
        </div>

        <div class="form-group">
            <label for="body">Body:</label>
            <textarea class="form-control" name="body" id="body">{{old('body')}}</textarea>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">Create</button>
        </div>

    </form>
@endsection